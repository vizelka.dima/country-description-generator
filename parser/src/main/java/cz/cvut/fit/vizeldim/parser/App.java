package cz.cvut.fit.vizeldim.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;


public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System. in);
        String url = scanner. nextLine();

        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Cant read from website");
        }
        String country = "";
        if (doc != null)
            country = doc.selectFirst("span.region").text();

        Parser p = null;
        try {
            p = new Parser(country);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Cant init parser.");
        }

        if (p != null) {
            p.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            p.o("country", "name", country);

            Elements el = doc.select("ul.expandcollapse li");
            for (int i = 0; i < el.size() - 1; i += 2) {
                String cat = el.get(i).selectFirst("div.question").text();
                p.o("category", "name", cat);

                Elements e = el.get(i + 1).select(">div");

                for (int j = 0; j < e.size() - 1; j += 2) {
                    String title = e.get(j).select(".category span.definition>a").text();
                    p.o("section", "name", title);
                    p.o("content");

                    Elements infos = e.get(j + 1).select(">div");


                    for (Element info : infos) {
                        p.o("info");
                        p.write(info.text().replace("<", "&lt;"));
                        p.c("info");
                    }


                    p.c("content");
                    p.c("section");
                }

                p.c("category");
            }

            p.c("country");

            p.Close();
        }
    }
}
