package cz.cvut.fit.vizeldim.parser;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Parser {
    PrintWriter out;
    public Parser(String fileName) throws FileNotFoundException {
        out = new PrintWriter(fileName+".xml");
    }
    public void Close(){
        out.close();
    }
    public void o(String tag){
        out.println("<" + tag + ">");
    }
    public void o(String tag, String att, String val){
        out.println("<" + tag + " " + att + "=\"" + val + "\"" + ">");
    }

    public void c(String tag){
        out.println("</" + tag + ">");
    }

    public void write(String text) {
        out.println(text);
    }
}
