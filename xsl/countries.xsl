<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<xsl:for-each select="./countries/country">
			<xsl:result-document href="html/{@name}.html">
				<html>
					<head>
						<meta charset="utf-8"/>
						<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
						<link rel="stylesheet" href="css/styles.css"/>
						<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"/>
						
						<title>
							<xsl:value-of select="@name"/>
						</title>
					</head>
					<body>
						<div id="sidebar">
							<div id="menu">
								<ul class="navbar-nav ">
									<xsl:for-each select="./category">
										<li class="nav-item">
											<a class="nav-link" href="#{@name}">
												<xsl:value-of select="@name"/>
											</a>
										</li>
									</xsl:for-each>
								</ul>
							</div>
						</div>
						
						<div id="myContainer">
							<h1 class="display-3">
								<xsl:value-of select="@name"/>
							</h1>
							<div class="galery">
								<img src="../source/images/{@name}-flag.png" alt=""/>
								<img src="../source/images/{@name}-locator-map.jpg" alt=""/>
							</div>
							<xsl:for-each select="./category">
								<div class="category my-3">
									<h2 id="{@name}" class="display-4">
										<xsl:value-of select="@name"/>
									</h2>
									<xsl:for-each select="./section">
										<div class="section">
											<h3 class="myh3">
												<xsl:value-of select="@name"/>
											</h3>
											<div class="content">
												<xsl:apply-templates select="./*" mode="normal"/>
											</div>
										</div>
									</xsl:for-each>
								</div>
								<hr/>
							</xsl:for-each>
						</div>
					</body>
				</html>
			</xsl:result-document>	
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="numeric-info" mode="normal">
		<div class="info">
			<xsl:apply-templates select="./@name" mode="normal"/>
			<xsl:apply-templates select="." mode="same"/>
		</div>
	</xsl:template>

	<xsl:template match="numeric-info" mode="sub">
		<xsl:apply-templates select="./@name" mode="sub"/>
		<xsl:apply-templates select="." mode="same"/>
	</xsl:template>

	<xsl:template match="numeric-info" mode="same">
		<xsl:value-of select="./@value"/>
		<xsl:apply-templates select="./@unit"/>
		<xsl:apply-templates select="./@year"/>
	</xsl:template>

	<xsl:template match="info/numeric-info">
		<xsl:apply-templates select="./@name" mode="sub"/>
		<xsl:apply-templates select="." mode="same"/>
		<xsl:text>; </xsl:text>
	</xsl:template>

	<xsl:template match="info" mode="normal">
		<div class="info">
			<xsl:apply-templates select="./@name"/>

			<xsl:apply-templates select="node()"/>

			<xsl:for-each select="./sub">
				<div class="sub">
					<xsl:apply-templates select="./@name"/>
					<xsl:value-of select="."/>
					<xsl:apply-templates select="./numeric-info" mode="sub"/>
				</div>	
			</xsl:for-each>
		</div>	
	</xsl:template>


	<xsl:template match="info/text()">
		<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="numeric-info/@name" mode="normal">
		<span class="title">
			<xsl:value-of select="."/>
			<xsl:text> : </xsl:text>
		</span>
	</xsl:template>

	<xsl:template match="numeric-info/@name" mode="sub">
		<span class="numeric-title">
			<xsl:value-of select="."/>
			<xsl:text> </xsl:text>
		</span>
	</xsl:template>

	<xsl:template match="numeric-info/@unit">
		<span class="unit">
			<xsl:text> </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text> </xsl:text>
		</span>
	</xsl:template>

	<xsl:template match="numeric-info/@year">
		<span class="year">
			<xsl:text> ( </xsl:text>
			<xsl:value-of select="."/>
			<xsl:text> ) </xsl:text>
		</span>
	</xsl:template>

	<xsl:template match="info/@name">
		<span class="title">
			<xsl:value-of select="."/>
			<xsl:text> : </xsl:text>
		</span>
	</xsl:template>

	<xsl:template match="sub/@name">
		<span class="subtitle">
			<xsl:value-of select="."/>
			<xsl:text> : </xsl:text>
		</span>
	</xsl:template>
</xsl:stylesheet> 