<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:result-document href="html/index.html">
			<html>
			<head>
				<meta charset="UTF-8"/>
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
				<title>Document</title>
				<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"/>
			</head>
			<body>
				<section class="jumbotron text-center">
				    <div class="container">
				      <h1>The World Factbook</h1>
				      <p class="lead text-muted">
				      	Semestral work for BI-XML
				      </p>
				    </div>
			  	</section>
				<div class="album py-3">
				    <div class="container">
				      	<div class="row">
				      		<xsl:for-each select="countries/country">
					       		<div class="col-md-3">
					          		<div class="card-img-top card mb-3 shadow-sm">
					          			<img width="100%" src="../source/images/{@name}-flag.png" alt=""/>
					            		<div class="card-body">
			   								<h4 class="card-title">
			   									<xsl:value-of select="@name"/>
			   								</h4>
											<p class="card-text">
												<xsl:value-of select="substring(./category[@name='Introduction'],0,100)"/>
												<br/>...
											</p>
			                				<a href="{@name}.html">
			                  					<button type="button" class="btn btn-sm btn-outline-secondary">
			                  						view
			                  					</button>
			                  				</a>
					            		</div>
					          		</div>
					        	</div>
				        	</xsl:for-each>
			 			</div> 
				 	</div>
			 	</div>      
			</body>
			</html>
		</xsl:result-document>
	</xsl:template>
</xsl:stylesheet>